import Vue from "vue";
import Router from "vue-router";
import First from "./views/First";
import Second from "./views/Second"
import Fourth from "./views/Fourth"
import Third from "./views/Third";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "first",
      component: First
    },
    {
      path: "/second",
      name: "second",
      component: Second
    },
    {
      path: "/third",
      name: "third",
      component: Third
    },
    {
      path: "/fourth",
      name: "fourth",
      component: Fourth
    },

  ]
});
